# NeuralJazz Movie Viewer

## Installation

Node v10.15.3にて開発しました。

```
cd movie
yarn install
```

## 映像データについて
400MBほどあるので、git管理から外しています。

`./movie/public/videos`に別途格納をお願いします。


## Usage

下記のコマンドで

* 映像表示ウインドウ
* 操作UI
* バックエンドのwebsocketサーバー
* OSC送信プログラム

が起動します。

```
cd movie
./start.sh
```

## Portについて

下記のポートを利用します。

* `127.0.0.1:3000` UIを動かすwebアプリ用のサーバー
* `127.0.0.1:5001` UIとバックエンドがデータをやり取りするためのwebsocketサーバー
* `127.0.0.1:7400` に向けて、データを送信

ウェブサーバーを立てているため、下記のURLを叩くことでもUIを呼び出せます。

* `http://localhost:3000/screen` 映像表示
* `http://localhost:3000/controller` 操作パネル、特徴量確認

## OSC通信について

`127.0.0.1:7400`の`/feat` に、長さ40の`float`型の配列を送信します。

配列の各次元の意味は`./movie/src/labels.json`に記されており、以下の値を表わします。

| Index | Name |
|-:|:-|
| 0 | amusement |
| 1 | awe |
| 2 | anger |
| 3 | contentment |
| 4 | disgust |
| 5 | excitement |
| 6 | fear |
| 7 | sadness |

また、`127.0.0.1:7400`の`/on-video-changed` に、映像の切り替えのトリガー信号を送ります。  
スムージングによる感情値の値動きの遅延を反映して、映像の切り替えから**3秒後**にトリガー信号を送信します。
