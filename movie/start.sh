#!/bin/bash

node hub.js &
BROWSER=none yarn start &
{ sleep 10; yarn electron; } &

trap 'kill $(jobs -p)' EXIT

wait

