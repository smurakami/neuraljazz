const server = require("ws").Server;
const s = new server({ port: 5001 });
const uuid = require('uuid');

var osc = require('node-osc');
var oscClient = new osc.Client('127.0.0.1', 7400);

let connections = [];

process.on('uncaughtException', function(err) {
    console.log(err);
});

s.on("connection", ws => {
  connections.push(ws);
  ws.on('close', () => {
    connections = connections.filter(function (conn, i) {
        return (conn === ws) ? false : true;
    });
  })

  ws.on("message", message => {
    try {
      let data = JSON.parse(message)
      if (data.event == 'onframe') {
        sendOSCFeat(data.data);
      }

      if (data.event == 'on-video-change') {
        sendOSCOnVideoChanged(data.data);
      }

      broadcast(message);
    } catch(error) {
      console.log(error)
    }
  });
});


function broadcast(message) {
  connections.forEach(function (con, i) {
      con.send(message);
  });
};

function sendOSCFeat(data) {
  let feat = data.feat;
  console.log(feat)
  oscClient.send('/feat', feat);
}

function sendOSCOnVideoChanged(data) {
  oscClient.send('/on-video-changed');
}
