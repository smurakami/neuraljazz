import React, {Component} from 'react';
import { BrowserRouter, Route, Link } from 'react-router-dom'

import logo from './logo.svg';
import './App.css';
import Screen from './Screen'
import Controller from './Controller'
import Conductor from './Conductor'

import VideoThumbnail from 'react-video-thumbnail'; // use npm published version


const App = () => (
  <BrowserRouter>
    <Route exact path='/screen' component={Screen} />
    <Route exact path='/controller' component={Controller} />
    <Route exact path='/conductor' component={Conductor} />
  </BrowserRouter>
  )

export default App;
