export default class GaussianFilter {
  constructor({variance, kernel_size}) {
    this.variance = variance;
    this.kernel_size = kernel_size;
    this.buffer = [];
    this.kernel = gaussianKernel(0, variance, kernel_size);
  }

  update(value) {
    this.buffer.push(value);
    if (this.buffer.length > this.kernel_size) {
      this.buffer = this.buffer.slice(1);
    }
  }

  value() {
    const {
      kernel,
      buffer,
    } = this;

    const size = Math.min(buffer.length, kernel.length);

    let result = []
    for (let dim = 0; dim < buffer[0].length; dim++) {
      let xs = [];
      for (let i = 0; i < size; i++) {
        xs.push(buffer[buffer.length - 1 - i][dim]);
      }
      let value = sum(zip(xs, kernel).map(e => e[0] * e[1])) / sum(kernel.slice(0, size));
      result.push(value);
    }

    return result;
  }
}

// 正規分布曲線
function norm(x, center=0, v=1) {
  x = (x - center);
  x = x / v;

  return (Math.exp(-x*x/2)) / Math.sqrt(2*Math.PI);
}

// 正規分布曲線フィルタ
function gaussianKernel(center, variance, size=20) {
  return Array.from({length: size}).map((v, i) => norm(i, center, variance));
}

function sum(array) {
  return array.reduce((s, e) => s + e);
}

function zip(a, b) {
  return Array
    .from({length: Math.min(a.length, b.length)})
    .map((v, i) => [a[i], b[i]]);
}

