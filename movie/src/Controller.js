import React, { Component } from 'react';
import './Controller.scss'
import Socket from './Socket'
import video_list from './feat_emo'
import labels from './labels_emo'


function basename(filepath) {
  filepath = filepath.split('/');
  return filepath[filepath.length - 1];
}


function argsort(array) {
    const arrayObject = array.map((value, idx) => { return { value, idx }; });
    arrayObject.sort((a, b) => {
        if (a.value < b.value) {
            return 1;
        }
        if (a.value > b.value) {
            return -1;
        }
        return 0;
    });
    const argIndices = arrayObject.map(data => data.idx);
    return argIndices;
}


export default class Controller extends Component {
  constructor() {
    super();
    this.socket = new Socket()
    this.socket.onMessage(data => this.onMessage(data))

    this.state = {
      file: null,
      pos: null,
      duration: null,
      feat: null,
      video_index: null,
      boost_mode: false,
      adlib_mode: false,
      filter_sec: 3.0,
    };
  }

  onMessage(data) {
    if (data.event == 'onframe') {
      this.onFrame(data.data);
    }

    if (data.event == 'set-boost-mode') {
      this.setState({boost_mode: data.data});
    }

    if (data.event == 'set-adlib-mode') {
      this.setState({adlib_mode: data.data});
    }
  }

  onFrame(data) {
    console.log(data);
    this.setState({
      file: data.file,
      pos: data.pos,
      duration: data.duration,
      feat: data.feat,
      video_index: data.video_index,
    })
  }

  onMovieButtonClick(index) {
    this.socket.send({
      event: 'set-movie',
      data: index,
    })
  }

  onControlButtonClick(control) {
    this.socket.send({
      event: 'control',
      data: control
    })
  }

  onFilterSliderChange(e) {
    const value = Number(e.target.value);

    this.socket.send({
      event: 'set-filter-sec',
      data: value,
    })

    this.setState({
      filter_sec: value,
    })
  }

  onBoostButtonChange(e) {
    const {checked} = e.target;

    this.socket.send({
      event: 'set-boost-mode',
      data: checked,
    })
  }

  onAdlibButtonChange(e) {
    const {checked} = e.target;

    this.socket.send({
      event: 'set-adlib-mode',
      data: checked,
    })
  }

  featTable(feat, sort=true) {
    if (!feat) {
      return (<div></div>)
    }

    let idx;
    if (sort) {
      idx = argsort(feat);
    } else {
      idx = feat.map((v, i) => i);
    }

    return (
        <table className="feat-table striped">
          <thead>
            <tr>
              <th>label</th>
              <th>value</th>
              <th><div className="bar-head"></div></th>
            </tr>
          </thead>
          <tbody>
            { idx.map((index, i)=> 
              <tr key={i}>
                <td>{labels[index]}</td>
                <td>{feat[index].toFixed(3)}</td>
                <td>
                  <div className="bar-body">
                    <div className="bar-body-inner" style={{width: feat[index] * 100 + '%'}}></div>
                  </div>
                </td>
              </tr>
              )
            }
          </tbody>
        </table>
      );
  }

  render() {
    const {
      file,
      duration,
      pos,
      feat,
      filter_sec, 
      boost_mode,
      adlib_mode,
    } = this.state;

    return (
      <div className="Controller">
        <div className="container">
          <div className="filename">
            <p>
              {this.state.file}
            </p>
          </div>

          <div className="row videos">
          { video_list.map((data, key) => 
            <div 
              className={[
                "video", "btn", file == data.file ? "red darken-2" : ''].join(' ')} 
              key={key}
              onClick={() => this.onMovieButtonClick(key)}
            >
            {basename(data.file).split('.')[0]}
            </div>
           ) }
          </div>

          <div className="row controls">

            <div 
              className={["control", "btn"] .join(' ')}
              onClick={() => this.onControlButtonClick("play")}
            ><i className="material-icons left">play_arrow</i>play</div>

            <div 
              className={["control", "btn"] .join(' ')}
              onClick={() => this.onControlButtonClick("pause")}
            ><i className="material-icons left">pause</i>pause</div>

          </div>

          <p className="range-field">
            <input type="range" id="test5" min="0" max="1" step="0.01" value={pos / duration} />
          </p>

          <p className="boost-button">
            <label>
              <input 
                type="checkbox" 
                onChange={this.onBoostButtonChange.bind(this)}
                checked={boost_mode}
              />
              <span>Boost</span>
            </label>
          </p>

          <p className="adlib-button">
            <label>
              <input 
                type="checkbox" 
                onChange={this.onAdlibButtonChange.bind(this)}
                checked={adlib_mode}
              />
              <span>ad-lib</span>
            </label>
          </p>

          <div className="feat">
          {this.featTable(feat)}
          </div>

          <div className="filter">
            <div className="row">
              <div className="col s6">
                <p className="title">スムージング秒数: {filter_sec.toFixed(2)}</p>
                <p className="range-field">
                  <input type="range" id="test6" min="0.1" max="10" step="0.01" value={filter_sec} onChange={this.onFilterSliderChange.bind(this)} />
                </p>
              </div>
            </div>
          </div>
        </div>
      </div>
    )
  }
}