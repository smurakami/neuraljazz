export default class Socket {
  constructor() {
    this.socket = null;
    this.connect();
    this.counter = 0;
    this.callbacks = {};
  }

  connect() {
    const socket = new WebSocket("ws://127.0.0.1:5001");

    socket.addEventListener("open", e => {
      console.log('WebSocket: opened');
    });

    socket.addEventListener("message", e => {
      this.__onMessage(e.data);
    });

    socket.addEventListener("close", e => {
      console.log('websocket: closed')
      // setTimeout(() => this.connect(), 1000);
    });

    socket.addEventListener("error", e => {
      console.log('websocket: error')
      // setTimeout(() => this.connect(), 1000);
    });

    this.socket = socket;
  }

  __onMessage(data) {
    data = JSON.parse(data);

    for (let key in this.callbacks) {
      this.callbacks[key](data);
    }
  }

  onMessage(callback) {
    const identifier = this.counter;
    this.counter++;

    this.callbacks[identifier] = callback;
    return identifier;
  }

  offMessage(identifier) {
    delete this.callbacks[identifier];
  }

  send(data) {
    if (WebSocket.OPEN != this.socket.readyState) {
      console.log('WebSocket/send: not opened')
      return false;
    }
    data = JSON.stringify(data);
    this.socket.send(data);
  }
}
