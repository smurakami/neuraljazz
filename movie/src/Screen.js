import React, { Component } from 'react';
import './Screen.scss'

import feat_data from './feat_emo'
import Socket from './Socket'

import GaussianFilter from './GaussianFilter'

function basename(filepath) {
  filepath = filepath.split('/');
  return filepath[filepath.length - 1];
}

const DEFAULT_KERNEL_SIZE = 100;

export default class Screen extends Component {
  constructor(props) {
    super(props);

    this.filter = new GaussianFilter({
      variance: 30,
      kernel_size: DEFAULT_KERNEL_SIZE
      });

    this.timer = null;
    this.socket = new Socket();
    this.socket.onMessage((data) => this.onMessage(data))
    this.prev_video_index = null;
    this.state = {
      video_index: 0,
      pos: 0,
      boost_mode: false,
      // label: null,
    };
  }

  componentDidMount() {
    const video = this.refs.video;

    video.addEventListener('ended', () => {
      this.onVideoEnd();
    })
    video.play();

    this.timer = setInterval(() => {
      this.onFrame();
    }, 100);
  }

  onMessage(data) {
    const video = this.refs.video;
    if (data.event == 'set-movie') {
      this.setState({
        video_index: data.data
      })
      video.play();
    }

    if (data.event == 'set-filter-sec') {
      let value = data.data;
      this.filter = new GaussianFilter({
        variance: value * 10,
        kernel_size: DEFAULT_KERNEL_SIZE,
      })
    }

    if (data.event == 'set-boost-mode') {
      this.setState({boost_mode: data.data});
    }

    if (data.event == 'control') {
      if (data.data == 'play') {
        video.play();
      }

      if (data.data == 'pause') {
        video.pause();
      }
    }
  }

  onFrame() {
    const {video_index} = this.state;
    const video_data = feat_data[video_index];
    const video = this.refs.video;
    const pos = video.currentTime;
    const feat = this.getFeat(pos);

    if (video_index != this.prev_video_index) {
      this.onVideoChange();
      this.prev_video_index = video_index;
    }

    if (video.paused || video.ended) {
      // 停止中は送らない
      return;
    }

    this.socket.send({
      event: 'onframe',
      data: {
        pos, feat, video_index,
        duration: video.duration,
        file: video_data.file,
      },
    });

    this.setState({
      pos: video.currentTime,
    });
  }

  onVideoChange() {
    const delay = 3000;
    setTimeout(() => {
      this.socket.send({
        event: 'on-video-change',
        data: {},
      })
    }, delay)
  }

  onVideoEnd() {
    let {video_index} = this.state;
    const video = this.refs.video;
    video_index += 1;
    if (video_index >= feat_data.length - 1) {
      video_index = 0;
    }

    this.setState({video_index})
    video.play();
  }

  getFeat(pos) {
    const {video_index, boost_mode} = this.state;
    const video_data = feat_data[video_index];

    const feat = video_data.pred;
    const timestamps = video_data.ts;

    let index = 0;
    for (var i = 0; i < timestamps.length; i++) {
      index = i;
      if (timestamps[i] >= pos) {
        break;
      }
    }

    if (index > feat.length - 1) {
      index = feat.length - 1;
    }

    let current = feat[index];
    this.filter.update(current);

    let value = this.filter.value();

    if (boost_mode) {
      value = this.boost(value);
    }

    return value;
  }

  boost(value) {
    const max = Math.max(...value);
    const index = value.indexOf(max);
    for (var i = 0; i < value.length; i++) {;
      if (index == i) {
        value[i] = 1.0;
      } else {
        value[i] = 0.0;
      }
    }
    return value;
  }

  componentWillUnmount() {
    if (this.timer) {
      clearInterval(this.timer);
      this.timer = null;
    }
  }  

  render() {
    const {video_index} = this.state;
    const video_data = feat_data[video_index];

    const video_url = video_data.file.replace('./', '/');

    return (
      <div className="Screen">
        <div className="col s6 video">
          <video src={video_url} controls={false} muted="muted" ref='video'></video>
        </div>

        {/*
          <div className="col s6">
            <div className="label-table">
              {this.labelTable(labels) }
            </div>

            <hr/>

            <div className="sound-table">
              {this.soundTable(labels) }
            </div>
          </div>
        */}
    </div>
    )
  }
}
