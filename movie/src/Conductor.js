import React, { Component } from 'react';
import {Radar} from 'react-chartjs-2';
import Socket from './Socket'
import "./Conductor.scss"

import labels from './labels_emo'

function changeOrder(array) {
  const order = [
    "excitement",
    "amusement",
    "awe",
    "contentment",
    "sadness",
    "disgust",
    "fear",
    "anger",
  ];

  let idx = [];
  for (let emotion of order) {
    idx.push(labels.indexOf(emotion));
  }

  let changed = [];
  for (let i of idx) {
    changed.push(array[i]);
  }

  return changed;
}

const chart_data = {
  labels: changeOrder(labels),
  datasets: [
    {
      label: '',
      backgroundColor: 'rgba(255,255,255,0.2)',
      borderColor: '#ecf0f1',
      // pointBackgroundColor: 'rgba(179,181,198,1)',
      // pointBorderColor: '#fff',
      // pointHoverBackgroundColor: '#fff',
      // pointHoverBorderColor: 'rgba(179,181,198,1)',
      data: [0, 0, 0, 0, 0, 0, 0, 0],
    },
  ]
};

const color_table = {
  "excitement": "#e67e22",
  "amusement": "#f1c40f",
  "awe": "#1abc9c",
  "contentment": "#2ecc71",
  "sadness": "#3498db",
  "disgust": "#8e44ad",
  "fear": "#2c3e50",
  "anger": "#c0392b",
}


const options =  {
  legend: {
    display: false,
  },
  scale: {
    ticks: {
      beginAtZero: true,
      max: 1,
      min: -0.2,
      fontSize: 32,
      display: false,
    },
    pointLabels: {       // 軸のラベル（"国語"など）
        fontSize: 18,         // 文字の大きさ
        fontColor: "white"    // 文字の色
    },
    gridLines: {
      color: "white",
    },
    angleLines: {
      color: "white",
    },
  }
}

export default class Conductor extends Component {
  constructor() {
    super();
    this.socket = new Socket()
    this.socket.onMessage(data => this.onMessage(data))

    this.state = {
      data: [0.65, 0.59, 0.90, 0.81, 0.56, 0.55, 0.40, 0.40],
      adlib_mode: false,
      adlib_flag: 0,
    }
  }

  componentDidMount() {
    setInterval(() => {
      this.setState({adlib_flag: this.state.adlib_flag + 1})
    }, 100)
  }

  onMessage(data) {
    if (data.event == 'onframe') {
      this.onFrame(data.data);
    }

    if (data.event == 'set-adlib-mode') {
      this.setState({adlib_mode: data.data});
    }
  }

  onFrame(data) {
    this.setState({
      data: data.feat,
    })
  }

  getMaxEmotion() {
    const {data} = this.state;
    const index = data.indexOf(Math.max(...data));
    const en = labels[index];

    const trans = {
      "amusement": "娯楽",
      "excitement": "興奮",
      "awe": "荘厳",
      "contentment": "安堵",
      "sadness": "悲壮",
      "disgust": "嫌悪",
      "fear": "恐怖",
      "anger": "憤怒",
    }

    const ja = trans[en];

    return {en, ja};
  }

  render() {
    let {data, adlib_mode} = this.state;
    chart_data.datasets[0].data = changeOrder(data);

    const max_emotion = this.getMaxEmotion();

    return (
      <div className="Conductor" style={{
        minHeight: window.innerHeight,
        backgroundColor: color_table[max_emotion.en],
      }}>
        <div className="row">
          <div className="chart col s8">
            <Radar data={chart_data} options={options} />
          </div>
          <div className="emotion col s4">
            <div className="ja">{max_emotion.ja}</div>
            <div className="en">{max_emotion.en}</div>
          </div>
        </div>

        {
          adlib_mode
          ?
          <div className="adlib_mode" style={{
            opacity: (this.state.adlib_flag % 4 == 0) ? 0 : 1
          }}>AD-LIB</div>
          :
          null
        }

      </div>
    );
  }
}
