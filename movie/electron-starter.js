const electron = require('electron');
// Module to control application life.
const app = electron.app;
// Module to create native browser window.
const BrowserWindow = electron.BrowserWindow;

const path = require('path');
const url = require('url');

// Keep a global reference of the window object, if you don't, the window will
// be closed automatically when the JavaScript object is garbage collected.
// let mainWindow;

windows = {};

function createWindow(path) {
    // Create the browser window.
    w = new BrowserWindow({width: 1920, height: 1080});
    windows[path] = w;

    // and load the index.html of the app.
    w.loadURL('http://localhost:3000' + path);

    // Open the DevTools.
    // w.webContents.openDevTools();

    // Emitted when the w is closed.
    w.on('closed', function () {
        // Dereference the w object, usually you would store windows
        // in an array if your app supports multi windows, this is the time
        // when you should delete the corresponding element.
        windows[path] = null;
    })


}

// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
// Some APIs can only be used after this event occurs.
app.on('ready', () => {
    for (let path of ['/screen', '/controller']) {
        if (!windows[path]) {
            createWindow(path)
        }
    }
});

// Quit when all windows are closed.
app.on('window-all-closed', function () {
    // On OS X it is common for applications and their menu bar
    // to stay active until the user quits explicitly with Cmd + Q
    if (process.platform !== 'darwin') {
        app.quit()
    }
});

app.on('activate', function () {
    // On OS X it's common to re-create a window in the app when the
    // dock icon is clicked and there are no other windows open.
    for (let path of ['/screen', '/controller']) {
        if (!windows[path]) {
            createWindow(path)
        }
    }
});

// In this file you can include the rest of your app's specific main process
// code. You can also put them in separate files and require them here.
